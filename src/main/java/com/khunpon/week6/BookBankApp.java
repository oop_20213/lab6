package com.khunpon.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank worawit = new BookBank("Worawit", 100.0);
        worawit.print();
        worawit.deposit(50);
        worawit.print();
        worawit.withdraw(50);
        worawit.print();

        BookBank prayood = new BookBank("Prayood", 1);
        prayood.deposit(100000);
        prayood.withdraw(1000000);
        prayood.print();

        BookBank praweet = new BookBank("Praweet", 10);
        praweet.deposit(1000000);
        praweet.withdraw(100000);
        praweet.print();
    }
}
